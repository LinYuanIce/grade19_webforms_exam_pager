﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace WebApplication1
{
    public partial class _Default : Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack) {
                FillDate(); 
            }
        }

        private void FillDate()
        {
            string Select = "select Title,Content,Author from Articles";
            var dt = Dbhelp.GetData(Select);
            GridView2.DataSource = dt;
            GridView2.DataBind();
        }

        protected void Button1_Click(object sender, EventArgs e)
        {
            var IsTitle = TextBox1.Text.Trim();
            var IsContent = TextBox2.Text.Trim();
            var IsAuthor = TextBox3.Text.Trim();
            var IsRemarks = TextBox4.Text.Trim();
            var Select = string.Format("select * from Articles where Title = '{0}'and Content = '{1}'and Author = '{2}'",IsTitle,IsContent,IsAuthor);
            var I = Dbhelp.GetData(Select);
            if (I.Rows.Count == 1)
            {
                Response.Write("该内容已存在,无法重复增加");
            }
            else {
                var Insert = string.Format("insert into Articles (Title,Content,Author,Remarks) values ('{0}','{1}','{2}','{3}')", IsTitle, IsContent, IsAuthor, IsRemarks);
                var IsInsert = Dbhelp.GetData(Insert);
                Response.Write("内容已增加");
                Response.Redirect("Default.aspx");
            }
        }
        protected void GridView2_SelectedIndexChanged(object sender, EventArgs e)
        {

        }
    }
}