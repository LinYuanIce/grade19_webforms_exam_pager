﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="Login.aspx.cs" Inherits="WebApplication1.Login" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
<meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
    <title>登录</title>
</head>
<body>
    <form id="form1" runat="server">
        <div>
            <table style="margin:auto;height:75px">
                <tr>
                    <td>
                        <asp:Label ID="Label1" runat="server" Text="用户名:"></asp:Label>
                    </td>
                    <td>
                        <asp:TextBox ID="TextBox1" runat="server"></asp:TextBox>
                    </td>
                </tr>
                <tr>
                    <td>
                        <asp:Label ID="Label2" runat="server" Text="密&nbsp;&nbsp;&nbsp;码:"></asp:Label>
                    </td>
                    <td>
                        <asp:TextBox ID="TextBox2" runat="server" TextMode="Password"></asp:TextBox>
                    </td>
                </tr>
                <tr>
                    <td>
                        <asp:Button style="margin-left:35px" ID="Button1" runat="server" Text="登录" OnClick="Button1_Click" />
                    </td>
                    <td>
                        <asp:Button style="margin-left:95px" ID="Button2" runat="server" Text="注册" OnClick="Button2_Click" />
                    </td>
                </tr>
            </table>

            
        </div>
    </form>
</body>
</html>
