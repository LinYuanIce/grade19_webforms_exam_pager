create database CMS
go
use CMS
go
create table Articles
(
	Id int primary key not null identity,
	Title nvarchar(80) not null,
	Content nvarchar(max) null,
	Author nvarchar(80) not null,
	IsActived bit not null default 1,
	IsDeleted bit not null default 0,
	CreatedTime datetime not null default getdate(),
	UpdatedTime datetime not null default getdate(),
	Remarks nvarchar(800) null
)
go
select * from Articles
create table Users
(
	Id int primary key not null identity,
	Username nvarchar(80) not null,
	Password nvarchar(max) null,
	IsActived bit not null default 1,
	IsDeleted bit not null default 0,
	CreatedTime datetime not null default getdate(),
	UpdatedTime datetime not null default getdate(),
	Remarks nvarchar(800) null
)
go
select * from Users
insert into Articles (Title,Content,Author) values ('你上头条啦','是什么重大新闻居然上头条？','老胡来啦')
,('一个老虎','有没有耳朵不知道','不知')
,('入门级显卡了解一下？','好用的不得了，经济实惠','佚名')
,('十一结婚太贵了','为什么贵，这我哪知道','国际小喇叭')
,('敞篷机你见过吗？','见也没见过，请指教','国内小喇叭')
go

insert into Users (Username,Password) values ('admin','123456'),('user01','123456')
go